# 十一作业

## 一、自学 Java
- 快速学习指南：[https://learnxinyminutes.com/docs/java/](https://learnxinyminutes.com/docs/java/)
- 课程：[https://www.w3cschool.cn/java/java-environment-setup.html](https://www.w3cschool.cn/java/java-environment-setup.html)
- 游戏：[https://www.codingame.com/ide/puzzle/onboarding](https://www.codingame.com/ide/puzzle/onboarding)

## 二、熟练掌握 git 命令
- 在 [https://gitee.com/traing-for-new-grads](https://gitee.com/traing-for-new-grads) 中创建自己的代码库

## 三、完成需求
要求：
1. 复制 [https://gitee.com/traing-for-new-grads/new-grads-homework](https://gitee.com/traing-for-new-grads/new-grads-homework) 的代码，并提交到自己新建的代码库
   - 代码库命名规范：XXX_homework （其中 XXX 是你的姓名全拼）
2. 小步提交的完成下列需求
3. 完成全部的用户故事
4. 实现方法/方式不限，但要有自己的设计原因和逻辑，并用某种方式描述清楚（可以包括但不限于：文档、注释等）

### 用户故事1: 重疾险产品
```text
作为一名初为人父/母的父亲/母亲
我想为自己不满1岁的儿子/女儿投保重疾险产品50万保额
想获悉所需每年投保费用并投保，以便子女在遭受身故、疾病时得到保险保障

AC1 为儿子购买该儿童重疾险
Given 保费按20年交，保障终身，0岁男宝宝投保保费为3090元/年
When 投保人为被保人（儿子）选择了该保险产品时
Then 客户可知悉需要为其缴纳3090元每年投保费用

AC2 为女儿购买该儿童重疾险
Given 保费按20年交，保障终身，0岁女宝宝投保保费为2970元/年
When 投保人为被保人（女儿）选择了该保险产品时
Then 客户可知悉需要为其缴纳2970元每年投保费用
```

### 用户故事2: 财产险产品
```text
作为房屋产权所有人
我想为自己名下房产投保普通险
想获悉每年所需投保费用并投保，以便在遭受火灾、雷击、爆炸、飞行器坠落等意外时得到保险保障

AC1 为房屋购买普通险（主险）
Given 普通家庭财产保险中为房屋投保普通险的，一等建筑(钢骨、水泥、砖石结构)，费率为1‰（每年）
客户p001名下房产评估价为600万，需要为其名下房产购买该保险（1年）
When 客户在选择了该保险产品时
Then 客户可知悉需要为其缴纳6000（6,000,000 * 1 / 1000）元投保费用
```

### 用户故事3: 健康险产品
```text
作为投保人
我想为自己或者家人购买长期医疗保险100万保额，想获悉所需每年投保费用并投保
以便在疾病时获得保险保障（需缴纳 100+（年龄-20）*100元/年 的投保费用），小于 20 岁不予通过

AC1 为自己或者家人购买长期医疗保险
Given 投保人为被保人选择了该保险产品
When 被保人为 40 岁
Then 客户可知悉需要为其缴纳 2100 元的投保费用（100 + （40 - 20）* 100）

AC2 为自己或者家人购买长期医疗保险
Given 投保人为被保人选择了该保险产品
When 被保人为 15 岁
Then 会抛出异常，并提示“年龄不符合投保规则，不能投保。”
```