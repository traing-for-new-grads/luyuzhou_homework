package com.example.homework1001;

public class PropertyInsurance {
    int year;
    int houseValue = 6000000;

    public PropertyInsurance(int y){
        this.year = y;
    }

    public void getProInsureCost(){
        //输出房屋投保费用及计算公式
        if(year <= 0)
            System.out.println("请输入大于0的整数数字！");
        else
            System.out.println(year+"年的房屋投保费用为:"+houseValue*year/1000+"("+houseValue+"*"+year+"/1000)元。");
    }
}
