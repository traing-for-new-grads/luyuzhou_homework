package com.example.homework1001;

public class HealthInsurance {
    private Person p;

    public HealthInsurance(Person p){
        this.p = p;
    }

    public void getHealInsureCost(){
        if (p.age < 20)
            //输出小于20岁客户无法投保说明
            System.out.println("年龄小于20岁无法投保本产品！");
        else if(p.age <= 120)
            //输出客户投保费用
            System.out.println("投保费用为："+(100+(p.age-20)*100)+"元(100+("+p.age+"-20)*100)");
        else
            System.out.println("请确认客户的年龄！");
    }
}
