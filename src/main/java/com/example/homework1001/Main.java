package com.example.homework1001;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        //展示保险产品种类供用户选择
        System.out.println("请输入想要购买保险种类的编号：");
        System.out.println("""
                重疾险产品编号：1
                财产险产品编号：2
                健康险产品编号：3""");

        //读取用户输入的保险产品品种
        Scanner scin = new Scanner(System.in);
        int insuranceKind = scin.nextInt();
        System.out.println(insuranceKind);

        //根据用户输入保险产品分类进入不同用户故事
        switch (insuranceKind) {
            case 1 -> {
                //进入重疾险用户故事
                System.out.println("您选择了重疾险产品，可为不满1岁的子女投保50万元保额的重疾险产品");
                System.out.println("请输入被保人性别，M(男孩)或F(女孩)，查看每年投保费用（保费按20年交）。");
                String gender = scin.next();
                Person child = new Person(0, gender);
                SicknessInsurance sickInsure = new SicknessInsurance(child);
                sickInsure.getAnnualInsurancePremium();
            }
            case 2 -> {
                //进入财产险用户故事
                System.out.println("您选择了财产险产品。");
                System.out.println("普通家庭财产保险中为房屋投保普通险的，费率为1‰（每年），请输入投保年数:");
                int year = scin.nextInt();
                PropertyInsurance proInsure = new PropertyInsurance(year);
                proInsure.getProInsureCost();
            }
            case 3 -> {
                //进入健康险用户故事
                System.out.println("您选择了健康险产品。");
                System.out.println("100万保额的长期医疗保险的每年投保费用为(100+（年龄-20）*100元)/年,请输入被保人年龄：");
                int age = scin.nextInt();
                Person p = new Person(age, "");
                HealthInsurance healInsure = new HealthInsurance(p);
                healInsure.getHealInsureCost();
            }
            default -> System.out.println("请输入正确的保险产品编号。");
        }

    }
}