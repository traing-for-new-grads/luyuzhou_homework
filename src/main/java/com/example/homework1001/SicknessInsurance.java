package com.example.homework1001;

public class SicknessInsurance {
    private Person p;
    int boyCost = 3090;
    int girlCost = 2970;

    public SicknessInsurance(Person p){
        this.p = p;
    }

    //根据输入的投保人信息，输出对应每年的投保费用；
    public void getAnnualInsurancePremium(){
        if (p.age >0)
            System.out.println("本重疾险只适用于不满一岁的儿童");
        else{
            if(p.gender.equals("M") || p.gender.equals("m"))
                //输出男孩的保费信息
                System.out.println("1岁以下男孩的重疾险每年投保费用为"+boyCost+"元。");
            else if(p.gender.equals("F") || p.gender.equals("f"))
                //输出女孩的保费信息
                System.out.println("1岁以下女孩的重疾险每年投保费用为"+girlCost+"元。");
            else
                System.out.println("请输入正确的性别字母。");
        }
    }
}
