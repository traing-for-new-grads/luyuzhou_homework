package com.example.homework_api.insurance.adapters.product.propertyInsurance;

import com.example.homework_api.insurance.adapters.RequestDto;

public class CalculatePropertyRequest implements RequestDto {
    public double value;
    public int year;
}
