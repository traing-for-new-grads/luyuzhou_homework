package com.example.homework_api.insurance.adapters.product.dreadDiseaseInsurance;

import com.example.homework_api.insurance.adapters.RequestDto;

public class CalculateDreadDiseaseRequest implements RequestDto {
    public String gender;
    public int age;
}
