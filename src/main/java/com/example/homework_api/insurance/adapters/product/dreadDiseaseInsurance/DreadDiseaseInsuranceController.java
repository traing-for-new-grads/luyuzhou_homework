package com.example.homework_api.insurance.adapters.product.dreadDiseaseInsurance;

import com.example.homework_api.insurance.adapters.product.InsuranceDto;
import com.example.homework_api.insurance.domain.Person;
import com.example.homework_api.insurance.domain.product.SicknessInsurance;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value = "/dread-disease-insurance", produces = APPLICATION_JSON_VALUE)
public class DreadDiseaseInsuranceController {
    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> post(@RequestBody CalculateDreadDiseaseRequest request) {
        SicknessInsurance dreadDiseaseInsurance = new SicknessInsurance();
        Person p = new Person(request.age, request.gender);
        double totalAmount;
        try {
            totalAmount = dreadDiseaseInsurance.calculate(p.gender,p.age);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok().body(new InsuranceDto(totalAmount));
    }
}
