package com.example.homework_api.insurance.adapters.product;

import com.example.homework_api.insurance.adapters.ResponseDto;

public class InsuranceDto implements ResponseDto {
    public double totalAmount;

    public InsuranceDto(double totalAmount) {
        this.totalAmount = totalAmount;
    }
}
