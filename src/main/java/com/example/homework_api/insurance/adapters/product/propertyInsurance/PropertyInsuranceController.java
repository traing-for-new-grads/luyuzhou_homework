package com.example.homework_api.insurance.adapters.product.propertyInsurance;

import com.example.homework_api.insurance.adapters.product.InsuranceDto;
import com.example.homework_api.insurance.domain.House;
import com.example.homework_api.insurance.domain.product.PropertyInsurance;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value = "/property-insurance", produces = APPLICATION_JSON_VALUE)
public class PropertyInsuranceController {
    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> post(@RequestBody CalculatePropertyRequest request) {
        PropertyInsurance propertyInsurance = new PropertyInsurance();
        House house = new House(request.value, request.year);
        double totalAmount;
        try {
            totalAmount = propertyInsurance.calculate(house.houseValue, house.year);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok().body(new InsuranceDto(totalAmount));
    }
}
