package com.example.homework_api.insurance.adapters.product.healthInsurance;

import com.example.homework_api.insurance.adapters.product.InsuranceDto;
import com.example.homework_api.insurance.domain.Person;
import com.example.homework_api.insurance.domain.product.HealthInsurance;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value = "/health-insurance", produces = APPLICATION_JSON_VALUE)
public class HealthInsuranceController {
    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> post(@RequestBody CalculateHealthRequest request) {
        HealthInsurance healthInsurance = new HealthInsurance();
        Person person = new Person(request.age,"");
        double totalAmount;
        try {
            totalAmount = healthInsurance.calculate(person.age);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok().body(new InsuranceDto(totalAmount));
    }
}
