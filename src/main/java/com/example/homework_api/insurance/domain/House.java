package com.example.homework_api.insurance.domain;

public class House {
    public double houseValue;
    public int year;

    public House(double value, int year){
        this.houseValue = value;
        this.year = year;
    }
}

