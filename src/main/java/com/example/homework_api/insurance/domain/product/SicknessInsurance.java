package com.example.homework_api.insurance.domain.product;

import com.example.homework_api.insurance.domain.InsuranceProduct;

public class SicknessInsurance extends InsuranceProduct<String> {
    @Override
    public double calculate(String gender){
            if(gender.equals("M") || gender.equals("m"))
                //输出男孩的保费信息
                return 3090;
            else if(gender.equals("F") || gender.equals("f"))
                //输出女孩的保费信息
                return 2970;
            else
                throw new IllegalArgumentException("请输入正确的性别字母。");

    }
    //根据输入的投保人信息，输出对应每年的投保费用；
    public double calculate(String gender ,double age ){
        if (age >0)
            throw new IllegalArgumentException("本重疾险只适用于不满一岁的儿童");
        else{
            if(gender.equals("M") || gender.equals("m"))
                //输出男孩的保费信息
                return 3090;
            else if(gender.equals("F") || gender.equals("f"))
                //输出女孩的保费信息
                return 2970;
            else
                throw new IllegalArgumentException("请输入正确的性别字母。");
        }
    }
}
