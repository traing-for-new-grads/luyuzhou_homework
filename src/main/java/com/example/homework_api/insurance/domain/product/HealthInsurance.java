package com.example.homework_api.insurance.domain.product;

import com.example.homework_api.insurance.domain.InsuranceProduct;
import com.example.homework_api.insurance.domain.Person;

public class HealthInsurance extends InsuranceProduct<Double> {
    public HealthInsurance() {
        this.amount = 1000000;
    }

    @Override
    public double calculate(Double age){
        if (age < 20 || age>120)
            //输出小于20岁或120岁以上客户无法投保说明
            throw new IllegalArgumentException("年龄不符合投保规则，不能投保。");
        else
            //输出客户投保费用
            return (100+(age-20)*100);
    }
}
