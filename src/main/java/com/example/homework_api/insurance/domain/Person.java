package com.example.homework_api.insurance.domain;

public class Person {
    public double age;
    public String gender;

    public Person(double age, String gender){
        this.age = age;
        this.gender = gender;
    }
}
