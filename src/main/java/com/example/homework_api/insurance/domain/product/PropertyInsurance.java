package com.example.homework_api.insurance.domain.product;

import com.example.homework_api.insurance.domain.InsuranceProduct;

public class PropertyInsurance extends InsuranceProduct<Double> {

    public double calculate(Double value){
        //输出房屋投保费用及计算公式
        if( value <=0)
            throw new IllegalArgumentException("房屋年限或价值不符合投保规则，不能投保。");
        else
            return value/1000;
    }

    public double calculate(Double value, int year){
        //输出房屋投保费用及计算公式
        if(year <= 0 || value <=0)
            throw new IllegalArgumentException("房屋年限或价值不符合投保规则，不能投保。");
        else
            return value*year/1000;
    }
}
