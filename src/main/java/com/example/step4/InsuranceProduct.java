package com.example.step4;

public interface InsuranceProduct<T> {
    double getAmount();
    double calculate(T param);
    //double calculate2(K param);

}