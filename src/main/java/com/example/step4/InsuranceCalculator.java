package com.example.step4;

import com.example.step4.product.*;

import static com.example.step4.product.DreadDiseaseInsurance.Gender.FEMALE;
import static com.example.step4.product.DreadDiseaseInsurance.Gender.MALE;

public class InsuranceCalculator {

    public static void main(String[] args) {

        InsuranceProduct dreadDiseaseInsurance = new DreadDiseaseInsurance();

        System.out.println(dreadDiseaseInsurance.getAmount() + "保费" + dreadDiseaseInsurance.calculate(MALE));
        System.out.println(dreadDiseaseInsurance.getAmount() + "保费" + dreadDiseaseInsurance.calculate(FEMALE));

        InsuranceProduct propertyInsurance = new PropertyInsurance();
        System.out.println(propertyInsurance.getAmount() + "保费" + propertyInsurance.calculate(6000000D));

        InsuranceProduct healthInsurance = new HealthInsurance(new Printer());
        System.out.println(healthInsurance.getAmount() + "保费" + healthInsurance.calculate(40));

        try {
            System.out.println("保费" + healthInsurance.calculate(15));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
