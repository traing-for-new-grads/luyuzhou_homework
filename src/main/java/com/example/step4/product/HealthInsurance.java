package com.example.step4.product;

import com.example.step4.InsuranceProduct;

public class HealthInsurance implements InsuranceProduct<Integer> {

    private String name = "大小没病 远离医生";
    private double amount;
    private Printer printer;

    public HealthInsurance(Printer printer) {
        this.amount = 1000000;
        this.printer = printer;
    }

    public HealthInsurance() {
        this.amount = 1000000;
    }

    @Override
    public double getAmount() {
        return amount;
    }

    @Override
    public double calculate(Integer age) {
        if (age < 20) {
            throw new IllegalArgumentException("年龄不符合投保规则，不能投保。");
        }
        //printer.print(age);
        return 100 + (age - 20) * 100;
    }

}
