package com.example.step4.product;

import com.example.step4.InsuranceProduct;

import java.math.BigDecimal;

public class PropertyInsurance implements InsuranceProduct<Double> {

    private String name = "资产平安 延续万年";
    private double amount;

    private BigDecimal rate = new BigDecimal("0.001");

    public PropertyInsurance() {
        this.amount = 6000000;
    }

    @Override
    public double getAmount() {
        return amount;
    }

    @Override
    public double calculate(Double value) {
        return BigDecimal.valueOf(value).multiply(rate).doubleValue();
    }

}
