package com.example.step4.product;

import com.example.step4.InsuranceProduct;

public class DreadDiseaseInsurance implements InsuranceProduct<DreadDiseaseInsurance.Gender> {

    private String name = "健康成长 一生无忧";
    private double amount;

    public DreadDiseaseInsurance() {
        this.amount = 500000;
    }

    @Override
    public double getAmount() {
        return amount;
    }

    @Override
    public double calculate(Gender gender) {
        if (Gender.MALE == gender) {
            return 3090;
        } else {
            return 2970;
        }
    }

    public enum Gender {
        MALE,
        FEMALE
    }
}
