package com.example.step4;

public abstract class InsuranceProductClass<T> {

    protected double amount;

    public double getAmount() {
        return amount;
    }

    public abstract double calculate(T param);
}