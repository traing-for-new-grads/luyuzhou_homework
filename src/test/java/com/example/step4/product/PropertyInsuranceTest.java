package com.example.step4.product;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class PropertyInsuranceTest {
    @Test
    void should_return_6000_when_input_is_6000000(){
        //given
        PropertyInsurance propertyInsurance = new PropertyInsurance();
        //when
        double totalMoney = propertyInsurance.calculate(6000000.0);
        //then
        assertThat(totalMoney).isEqualTo(6000.0);
    }
}
