package com.example.step4.product;

import org.junit.jupiter.api.Test;

import static com.example.step4.product.DreadDiseaseInsurance.Gender.FEMALE;
import static com.example.step4.product.DreadDiseaseInsurance.Gender.MALE;
import static org.assertj.core.api.Assertions.assertThat;

public class DreadDiseaseInsuranceTest {
    @Test
    void should_return_3090_when_input_gender_is_male(){
        //given
        DreadDiseaseInsurance dreadDiseaseInsurance = new DreadDiseaseInsurance();
        //when
        double totalMoney = dreadDiseaseInsurance.calculate(MALE);
        //then
        assertThat(totalMoney).isEqualTo(3090.0);
    }

    @Test
    void should_return_2970_when_input_gender_is_female(){
        //given
        DreadDiseaseInsurance diseaseInsurance = new DreadDiseaseInsurance();
        //when
        double totalMoney = diseaseInsurance.calculate(FEMALE);
        //then
        assertThat(totalMoney).isEqualTo(2970.0);
    }
}
