package com.example.step4.product;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class HealthInsuranceTest {
    @Test
    void should_return_2100_when_input_age_is_40(){
        //given
        HealthInsurance healthInsurance = new HealthInsurance(new Printer());
        //when
        double totalMoney = healthInsurance.calculate(40);
        //then
        assertThat(totalMoney).isEqualTo(2100.0);
    }

    @Test
    void should_return_reject_when_input_age_is_15(){
        //given
        HealthInsurance healthInsurance = new HealthInsurance(new Printer());
        //when
        try{
            double totalMoney = healthInsurance.calculate(15);
        }
        //then
        catch (Exception e){
            assertThat(e.getMessage()).isEqualTo("年龄不符合投保规则，不能投保。");
        }
    }
}
