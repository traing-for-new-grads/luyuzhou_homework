# 项目介绍
##利用postman进行api功能验证

## 一、保险产品分类
- 1 重疾险 sickness insurance 
- 2 财产险 property insurance
- 3 健康险 health insurance

## 二、交互流程
- 从postman中分别输入不同uri及参数，验证三种产品

## 三、各保险产品流程
### 用户故事1: 重疾险产品
```text
localhost:8080/dread-disease-insurance
jason格式：
{
 "age":0,
 "gender":"f"
}
输入年龄0，性别"f"，返回女孩的保费2970
输入年龄2，性别"m"，返回参保孩童年龄异常
```
![avatar](https://gitee.com/traing-for-new-grads/luyuzhou_homework/raw/master/sickness-insur-age0.png)
![avatar](https://gitee.com/traing-for-new-grads/luyuzhou_homework/raw/master/sickness-insur-age2.png)

### --------------------------------------------------------------------------------------------------------------------
### 用户故事2: 财产险产品
```text
localhost:8080/property-insurance
jason格式：
{
 "value":50000,
 "year":2
}
输入房屋价值50000，参保年险2，返回100的保费
输入房屋价值50000，参保年险0，返回房屋年限异常说明
```
![avatar](https://gitee.com/traing-for-new-grads/luyuzhou_homework/raw/master/property-insur-year2.png)
![avatar](https://gitee.com/traing-for-new-grads/luyuzhou_homework/raw/master/property-insur-year0.png)

### --------------------------------------------------------------------------------------------------------------------
### 用户故事3: 健康险产品
```text
localhost:8080/health-insurance
jason格式：
{
 "age":30
}
输入30，返回1100的保费
输入15，返回年龄异常说明
```
![avatar](https://gitee.com/traing-for-new-grads/luyuzhou_homework/raw/master/health-insur-age30.png)
![avatar](https://gitee.com/traing-for-new-grads/luyuzhou_homework/raw/master/health-insur-age15.png)